package visitors;

import components.creatures.Cow;
import components.creatures.Human;
import components.creatures.Octopus;

public class Helicopter extends FlyingObject {
    @Override
    public void visitAHuman(Human human) {
        if(human.hasProbe()) {
            System.out.println("Removing the probe from " + human.getName());
            human.takeOffProbe();
        } else {
            System.out.println("Cannot remove probe; " + human.getName() + " doesn't have one.");
        }
    }

    @Override
    public void visitACow(Cow cow) {
        if(!cow.hasBeenAbducted() && !cow.isTied()) {
            System.out.println("Tying the cow to a post");
            cow.tie();
        } else {
            if(cow.hasBeenAbducted())
                System.out.println("Cannot rescue this cow; it has been abducted.");
            else if(cow.isTied())
                System.out.println("This cow is already tied to a post");
        }
    }

    @Override
    public void visitAnOctopus(Octopus octopus) {
        System.out.println("That's an octopus.");
    }
}
