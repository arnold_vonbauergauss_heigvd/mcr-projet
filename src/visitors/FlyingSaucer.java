package visitors;

import components.creatures.Cow;
import components.creatures.Human;
import components.creatures.Octopus;

public class FlyingSaucer extends FlyingObject {
    private static final int OBJECTIVE_WEIGHT = 5000;

    private int weight;

    public FlyingSaucer() {
        weight = 0;
    }

    @Override
    public void visitAHuman(Human human) {
        System.out.println("Putting a probe to " + human.getName());
        human.putProbe();
    }

    @Override
    public void visitACow(Cow cow) {
        if(!cow.hasBeenAbducted() && !cow.isTied()) {
            System.out.println("Abducting a cow; weight is " + cow.getWeight());
            cow.abduct();
            weight += cow.getWeight();
        } else {
            if(cow.hasBeenAbducted())
                System.out.println("Cannot abduct this cow; has already been abducted !");
            else if(cow.isTied())
                System.out.println("Cannot abduct this cow, it is tied to a post.");
        }
    }

    @Override
    public void visitAnOctopus(Octopus octopus) {
        System.out.println("The aliens encountered an octopus");
    }
}
