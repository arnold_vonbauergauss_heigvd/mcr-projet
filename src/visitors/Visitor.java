package visitors;

import components.creatures.Cow;
import components.House;
import components.creatures.Human;
import components.creatures.Octopus;

public interface Visitor {
    void visitAHuman(Human human);
    void visitACow(Cow cow);
    void visitAnOctopus(Octopus octopus);
}
