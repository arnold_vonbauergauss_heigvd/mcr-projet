package components;

import visitors.Visitor;

public abstract class Entity {
    public abstract void accept(Visitor visitor);
}
