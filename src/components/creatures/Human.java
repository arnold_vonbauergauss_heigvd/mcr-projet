package components.creatures;

import visitors.Visitor;

public class Human extends Creature {
    private String name;
    private boolean hasProbe;

    public Human(String name) {
        this.name = name;
        hasProbe = false;
    }

    public String getName() {
        return name;
    }

    public boolean hasProbe() {
        return hasProbe;
    }

    public void putProbe() {
        hasProbe = true;
    }

    public void takeOffProbe() {
        hasProbe = false;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitAHuman(this);
    }

}
