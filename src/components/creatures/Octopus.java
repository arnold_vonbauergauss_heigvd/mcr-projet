package components.creatures;

import visitors.Visitor;

public class Octopus extends Creature {
    @Override
    public void accept(Visitor visitor) {
        visitor.visitAnOctopus(this);
    }
}
