package components.creatures;

import visitors.Visitor;

import java.util.Random;

public class Cow extends Creature {
    private static final int MIN_WEIGHT = 35;
    private static final int MAX_WEIGHT = 1000;

    private static final Random randy = new Random();

    private int weight;
    private boolean hasBeenAbducted;
    private boolean isTied;

    public Cow() {
        weight = randy.nextInt(MAX_WEIGHT - MIN_WEIGHT + 1) + MIN_WEIGHT;
        hasBeenAbducted = false;
        isTied = false;
    }

    public int getWeight() {
        return weight;
    }

    public void abduct() {
        hasBeenAbducted = true;
    }

    public boolean hasBeenAbducted() {
        return hasBeenAbducted;
    }

    public void tie() {
        isTied = true;
    }

    public boolean isTied() {
        return isTied;
    }

    @Override
    public void accept(Visitor visitor) {
        visitor.visitACow(this);
    }
}
