package components;

import components.creatures.Cow;
import components.creatures.Creature;
import components.creatures.Human;
import visitors.Visitor;

import java.util.LinkedList;
import java.util.List;

public class House extends Entity {
    private List<Creature> inhabitants;

    public House() {
        inhabitants = new LinkedList<>();
    }

    public void add(Creature creature) {
        inhabitants.add(creature);
    }

    public void remove(Creature creature) {
        inhabitants.remove(creature);
    }

    public void getChild(int i) {
        inhabitants.get(i);
    }

    @Override
    public void accept(Visitor visitor) {
        for(Creature creature : inhabitants)
            creature.accept(visitor);
    }
}
