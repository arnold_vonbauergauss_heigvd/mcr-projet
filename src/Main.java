import components.House;
import components.creatures.Cow;
import components.creatures.Human;
import components.creatures.Octopus;
import visitors.FlyingSaucer;
import visitors.Helicopter;

public class Main {
    public static void main(String[] args) {
        Human gerald = new Human("Gerald");
        Human brian = new Human("Brian");
        Human robert = new Human("Robert");
        Cow cow1 = new Cow();
        Cow cow2 = new Cow();
        Cow cow3 = new Cow();
        Octopus octodad = new Octopus();
        House house = new House();

        house.add(new Cow());
        house.add(new Human("George"));
        house.add(new Human("Fritz"));

        FlyingSaucer ufo = new FlyingSaucer();
        Helicopter rescue = new Helicopter();

        gerald.accept(ufo);
        brian.accept(ufo);
        robert.accept(rescue);
        gerald.accept(rescue);
        cow1.accept(ufo);
        cow2.accept(rescue);
        cow3.accept(rescue);
        cow2.accept(ufo);
        cow3.accept(ufo);
        octodad.accept(rescue);
        octodad.accept(ufo);
        house.accept(ufo);
        house.accept(rescue);
    }
}
